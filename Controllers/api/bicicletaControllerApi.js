var Bicicleta=require('../../Models/bicicleta');

exports.bicicleta_list=function(req,res){
    res.status(200).json({
        bicicleta: Bicicleta.allBicis
    });
}

exports.bicicleta_agregar=function(req , res ){
    var bici= new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion=[req.body.lat , req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_eliminar=function(req , res ){
    Bicicleta.removeById(req.params.id);
    res.status(200).send();
}

exports.bicicleta_modificar=function(req , res ){
    var bici= Bicicleta.findById(req.params.id);
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.lat , req.body.lng];
    res.status(200).json({
        bicicleta: bici
    });
    
}