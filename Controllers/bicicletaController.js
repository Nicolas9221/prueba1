var Bicicleta= require('../Models/bicicleta');

exports.bicicleta_list = function(req,res){
    res.render('bicicletas/index',{bicis: Bicicleta.allBicis});
}

exports.bicicletas_agregar_get=function(req,res){
    res.render('bicicletas/agregar');
}

exports.bicicletas_agregar_post=function(req,res){
    var bici= new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion=[req.body.lat , req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}
exports.bicicleta_eliminar_post=function(req,res){
  Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}


exports.bicicleta_modificar_get=function(req,res){
    var bici=Bicicleta.findById(req.params.id);
    res.render('bicicletas/modificar', {bici});
  }

exports.bicicleta_modificar_post=function(req,res){
    var bici=Bicicleta.findById(req.params.id);
    bici.id=req.body.id;
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.lat , req.body.lng];
    

    res.redirect('/bicicletas');
  }