var Bicicleta= require("../../../Models/bicicleta");
var mongoose=require('mongoose');
//"Jasmine" 
//npm test -> Te realiza todos los archivos de test.
// jasmine spec\support\models\bicicleta_test_spec.js ejecutas un test en concreto


describe('Testing Bicicletas', function(){ 
    beforeEach(function(done){//Configuración de la base de datos  
                            //Hasta que no se ejecute done el test no termina. Al js ser asincronico si no pongo el done podria terminar el test sin esperar la respuesta del request
        
        var mongoDB='mongodb://127.0.0.1:27017/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser:true});
        const db=mongoose.connection;
        db.on('error', console.error.bind(console, 'Coneection error: '));
        db.once('open', function(){
            console.log('Estamos conectado a las base de dato de testing: ');
            done();
        });

    });


    afterEach(function(done)
    { //Eliminamos todo, 'deleteMany es un comando de mongoose'
        Bicicleta.deleteMany({}, function(err,success){
            if(err) 
                console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance',() => {
        it('Creo una instancia de bicicleta', () => {
            var bici= Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis',() => {
        it('comienza vacia', (done) => {
           Bicicleta.allBicis(function(err,bicis){
            expect(bicis.lenght).toBe(0);
            done();
           });
        });
    });

    describe('Bicicleta.add',() => {
        it('agrega solo una bici', (done) => {
            var aBici= new Bicicleta({code:1,color:'verde', modelo:'urbana'});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.lenght).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });


    describe('Bicicleta.findByCode', () =>{
        it('debo devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.lenght).toBe(0);
                if(err) console.log(err);
                var aBici= new Bicicleta({code:1,color:'verde', modelo:'urbana'});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    
                    var aBici2= new Bicicleta({code:2,color:'verde', modelo:'urbana'});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1,function(err,targetBici){
                            expect(targetBici.code).toBe(1);
                            expect(targetBici.color).toBe('verde');
                            expect(targetBici.modelo).toBe('urbana');
                            done();
                        });    
                    });
                });
            });
        });
    });

});










/*
beforeEach(()=> { Bicicleta.allBicis=[]; }); //Antes de cada test... (Me vacia la lista)


describe('Bicicleta.allBicis',() => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe('Bicicleta.add',() =>{
    it('agregamos una bicicleta', () =>{
        expect(Bicicleta.allBicis.length).toBe(0); //Lo agrego nuevamente por si algún test previo modificó la lista(No es obligatorio..)./ Sería el esta previo(la precondición) que tendria que tener.. (con el beforForeach no seria tan necesario)
        
        var a= new Bicicleta(1,'rojo','urbana',[-34.860885, -56.205637]);
        Bicicleta.add(a);
        
        expect(Bicicleta.allBicis.length).toBe(1); //El estado posterior(la postcondición) que tendría que tener la lista luego del add.
        expect(Bicicleta.allBicis[0]).toBe(a); //Otra postcondición, que la única bici sea 'a'..
    })
});

describe('Bicicleta.findById',() => {
    it('debe devolver a bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var aBici1= new Bicicleta(1,"verde","urbana");
        var aBici2= new Bicicleta(2,"roja","montaña");
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici= Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);

    })
});

describe('Bicicleta.removeById',() => {
    it('debe eliminar la bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var aBici1= new Bicicleta(1,"verde","urbana");
        var aBici2= new Bicicleta(2,"roja","montaña");
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0].id).toBe(2);


    })
});
*/