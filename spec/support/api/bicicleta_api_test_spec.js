var Bicicleta= require('../../../Models/bicicleta');
var request = require('request'); //La debo instalar "npm install request --save" // Me permite ejecutar POST y GET para testear
var server= require('../../../bin/www'); // Lo necesito para levantar el server automaticamnete al iniciar el testeo


describe('Bicicleta API' , () =>{
    describe('GET BICICLETAS' , () =>{
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a= new Bicicleta(1,'rojo','urbana',[-34.860885, -56.205637]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST BICICLETA API /agregar' , () =>{
        it('status 200', (done) => { //Hasta que no se ejecute done el test no termina. Al js ser asincronico si no pongo el done podria terminar el test sin esperar la respuesta del request
            var header= {'content-type':'application/json'};
            var aBici= '{"id":10 , "color": "rojo" , "modelo":"urbano" , "lat":-34 , "lng":-34 }';
            request.post({
                headers:header,
                url: 'http://localhost:3000/api/bicicletas/agregar',
                body:aBici
            }, function(error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
});