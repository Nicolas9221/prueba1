var map = L.map('mapid').setView([-34.882171, -56.172974], 13);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
maxZoom: 18
}).addTo(map);

// L.marker([-34.906106, -56.194424],{draggable: true}).addTo(map);
// L.marker([-34.879085, -56.189244],{draggable: true}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicleta.forEach( function(bici) { //bicicleta es lo que devuelve en el controlador ((bicicleta: Bicicleta.allBicis))
            
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map); 
           
        })
    }
})