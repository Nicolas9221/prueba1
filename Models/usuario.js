var mongoose=require('mongoose');
var Reserva = require('./reserva');
const uniqueValidator=require('mongoose-unique-validator');//Modulo de mongoose que valida que sea unique
var Schema = mongoose.Schema;
const saltRounds=10;//Agrega aleatoridad a la encriptación
const bcrypt=require('bcrypt'); //Encripta el password 'npm install bcrypt --save' 

const validateEmail = function(email){
    const re= /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};


var usuarioSchema= new Schema({
    nombre:{
        type: String,
        trim:true,//Si hay espacios vacios lo quitará
        required: true [true, 'El nombre es obligatorio']//Obligatorio, y si sucede un error muestra ese mensaje//Viene por defecto pero es en ingles...

    },
    email:{
        type:String,
        trim:true,
        required: true [true, 'El email es obligatorio'],
        unique:true, //Valor único en la bd..Necesito instalar 'npm i moongose.validator --save'
        lowercase:true, //Pone todo en minúscula
        validate:[validateEmail,'Por favor ingrese un email valido'],
        match:[/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password:{
        type: String,
        trim:true,
        required: true [true, 'El password es obligatorio']

    },
    passwordResetToken:String,
    passwordResetTokenExpires:Date,
    verificado:{
        type:Boolean,
        default:false
    }

});

usuarioSchema.plugin(uniqueValidator,{message:'El{PATH} ya existe con otro usuario'});//Son modulos que no son parte de mongoose, por eso debo utilizo plugin para incorporarlas

usuarioSchema.pre('save',function(next){ //'pre': Funcion de mongo que se ejecuta antes de hacer la una función, en este caso 'save'
    if(this.isModified('password'))
    {
        this.password=bcrypt.hashSync(this.password, saltRounds) // Me encripta el password
    }
    next(); //Continua el save
});

usuarioSchema.methods.validPassword= function(password){ //En el login encripto el password ingresado para verificar que coincida con el password que está en la bd.
    return bcrypt.compareSync(password,this.password);
};

usuarioSchema.methods.reservar= function(bicicId, desde ,hasta ,cb){
    var reserva= new Reserva( {usuario: this._id, bicicleta: bicicId, desde:desde, hasta:hasta});
    console.log(reserva);
    reserva.save(cb);
}

module.exports= mongoose.model('Usuario',usuarioSchema);