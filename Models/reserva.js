var mongoose=require('mongoose');
var moment = require('moment');// Añade herramientas de fechas 'npm install moment'
var Schema = mongoose.Schema;

var reservaSchema=new Schema({ 
    desde: Date,
    hasta: Date,
    modelo: String,
    bicicleta:{type: mongoose.Schema.Types.ObjectId , ref:'Bicicleta'}, //Referencia a bicicleta
    usuario:{type: mongoose.Schema.Types.ObjectId , ref:'Usuario'}, // La ref 'Usuario sale del modelo usuario 'module.exports= mongoose.model('Usuario',usuarioSchema);'

});

reservaSchema.methods.diasDeReserva= function(){
    return moment(this.hasta).diff(moment(this.desde), 'days')+1;
}

//module.exports = mongoose.model('Reserva', reservaSchema);