const passport = require('passport');
const LocalStrategy=require('passport-local').Strategy;
const usuario=require('../Models/usuario');

passport.use(new LocalStrategy((email, password, done) => {
    usuario.findOne({email: email}, (err, usuario)=>{
        if(err){
            return done(err);
        }

        if(!usuario){
            return done(null, false, {message: 'Usuario o contraseña incorrectos'});
        }
        if(!usuario.validPassword(password)){
            return done(null, false, {message: 'Usuario o contraseña incorrectos'});
        }

        if(!usuario.verificado){
            return done(null, false, {message: 'Cuenta inactiva. Por favor verifiquela'});
        }

        return done(null, usuario);
    });
}

));

passport.serializeUser(function(user,cb){
    cb(null,user.id);
});

passport.deserializeUser(function(id,cb){
    usuario.findById(id,function(err,usuario){
        cb(err,usuario);
    });
});

module.exports=passport;