var express = require('express'); 
var router = express.Router();
var bicicletaController= require('../Controllers/bicicletaController');

router.get('/', bicicletaController.bicicleta_list);
router.get('/agregar', bicicletaController.bicicletas_agregar_get);
router.post('/agregar', bicicletaController.bicicletas_agregar_post);
router.post('/:id/eliminar', bicicletaController.bicicleta_eliminar_post);
router.get('/:id/modificar', bicicletaController.bicicleta_modificar_get);
router.post('/:id/modificar', bicicletaController.bicicleta_modificar_post);

module.exports= router;