var express = require('express'); 
var router = express.Router();
var bicicletaController= require('../../Controllers/api/bicicletaControllerApi');

router.get('/', bicicletaController.bicicleta_list );
router.post('/agregar', bicicletaController.bicicleta_agregar );
router.post('/:id/eliminar', bicicletaController.bicicleta_eliminar );
router.post('/:id/modificar', bicicletaController.bicicleta_modificar );

module.exports=router;